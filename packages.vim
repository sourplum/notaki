" Block adds
Plug 'gorhill/uBlock'
" Vim support
Plug 'akhodakivskiy/VimFx'
" UI customization
Plug 'Aris-t2/ClassicThemeRestorer'
" User JS scripts
Plug 'violentmonkey/violentmonkey'
" User CSS styles
Plug 'openstyles/stylus'
