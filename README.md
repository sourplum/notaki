# Notaki

Addons Package manager for Firefox and derivate browsers (Waterfox, Palemoon, Icecat...).

## Usage

+ `./packages.vim`

```vim
" List plugins to download from github repositories

" fetches https://github.com/gorhill/uBlock
" will download .xpi from latest release
Plug 'gorhill/uBlock'
```

+ `./script.sh`

Download `.xpi` plugins corresponding to `package.vim`.
Plugins are downloaded under `addons` subdirectory.

## Why

Account synchronization with Firefox requires a ridiculous amount of work.
No less than 3 applications are required just to sync a profile:
+ https://github.com/mozilla-services/syncserver
+ https://github.com/mozilla/fxa-auth-server/
+ https://github.com/mozilla/fxa-content-server/

While I could setup those services easily using docker-compose and Ansible. To think I add to use automation tools to simply share configuration!

Why can't we just share firefox extension as we share dotfiles configurations?

This package manager is heavily influenced by vim-plug for vim. It tries to mimic it's use with simple shell script. Less is more.
