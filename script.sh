#! /usr/bin/env sh

function list_plugins {
    cat packages.vim | awk '/^Plug/ {plug=substr($2,2,length($2)-2); print plug;}';
}

function get_latest_releases {
    curl -s https://api.github.com/repos/$plug/releases/latest;
}

function get_download_url {
    # $latest_releases | jq --raw-output ".assets[0] | .browser_download_url";
    $latest_releases | awk '/.xpi\"$/ {download=substr($2,2,length($2)-2); print download;}';
}

function print_app_id {
    unzip -po ./addons/$download | awk '/<em:id>/ {appid=substr($1,8,length($1)-15); print appid;}' | head -1;
}

function install_addons {
    # 烈火灼冰
    app_id=$(print_app_id);
    # extract_addon;
}

# Store list of plugins as an array
plugs=($(list_plugins))

# For each plugin, get download url and download
for plug in ${plugs[*]}
do
    latest_releases=get_latest_releases;
    download=($(get_download_url));
    download=${download[0]}
    wget -P ./addons $download
done
